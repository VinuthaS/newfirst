package com.servletcookies;

import java.io.PrintWriter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SecondServlet extends HttpServlet
{
	public void doPost(HttpServletRequest req,HttpServletResponse res)
	{
		try{
		res.setContentType("text/html");
		PrintWriter out=res.getWriter();
		
		Cookie ck[]=req.getCookies();
		out.print("Hello "+ck[0].getValue());  

		out.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		}
}
